INTRODUCTION
------------

XML Sitemap VBO module allows you to create a View to include Nodes, Taxonomy
Terms, and Users to a XML Sitemap via Views Bulk Operations.


REQUIREMENTS
------------

This module requires the following modules:
 * Views Bulk Operations (https://www.drupal.org/project/views_bulk_operations)
 * XML Sitemap (https://www.drupal.org/project/xmlsitemap)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7 for further
   information.


CONFIGURATION
-------------
 * Once the module is installed, create a View and you will be able to add two
   fields and two filters.
   * Status - If the item is included/excluded in the XML Sitemap.
   * Status Override - If the default XML sitemap inclusion is overriden (this
     is set at admin/structure/types/manage/page).


TROUBLESHOOTING
---------------

The module currently supports Nodes, Taxonomy Terms, and Users. Adding Menu
items currently is not supported.


MAINTAINERS
-----------

Nate Millin at WI Department of Public Instruction
(https://www.drupal.org/wi-department-of-public-instruction).
