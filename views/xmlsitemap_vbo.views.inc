<?php

/**
 * @file
 * Creating hooks so xmlsitemap fields can be added using views.
 */

/**
 * Implements hook_views_data().
 */
function xmlsitemap_vbo_views_data() {
  // Creating XML Sitemap Group.
  $data['xmlsitemap']['table']['group'] = t('XML Sitemap');

  // Define xmlsitemap as a base table.
  $data['xmlsitemap']['table']['base'] = array(
    // This is the identifier field for the view.
    'field' => 'id',
    'title' => t('xmlsitemap table'),
    'help' => t('Exposing xmlsitemap table settings to Views.'),
    'weight' => -10,
  );

  // This declaration below creates an implicit relationship to tables.
  $data['xmlsitemap']['table']['join'] = array(
    // The table references the {node} table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'id',
    ),
    // The table references the {taxonomy_term_data} table.
    'taxonomy_term_data' => array(
      'left_field' => 'tid',
      'field' => 'id',
    ),
    // The table references the {users} table.
    'users' => array(
      'left_field' => 'uid',
      'field' => 'id',
    ),
  );

  // Creating the status field.
  $data['xmlsitemap']['status'] = array(
    'title' => t('Status'),
    'help' => t('If the item is included in the XML Sitemap.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'label' => t('Status'),
      'handler' => 'views_handler_filter_boolean_operator',
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Creating the status override field.
  $data['xmlsitemap']['status_override'] = array(
    'title' => t('Status Override'),
    'help' => t('If the default settings are overridden.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'label' => t('Status Override'),
      'handler' => 'views_handler_filter_boolean_operator',
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}
