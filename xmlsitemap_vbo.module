<?php

/**
 * @file
 * This file creates the actions required to integrate with VBO.
 *
 * VBO actions tutorial (https://www.drupal.org/node/2052067) that helped
 * when creating this.
 */

/**
 * Implements hook_action_info().
 */
function xmlsitemap_vbo_action_info() {
  return array(
    // VBO Actions for Nodes.
    'xmlsitemap_vbo_node_add_to_xmlsitemap_action' => array(
      'type' => 'node',
      'label' => t('XML Sitemap Node: Include Node'),
      'behavior' => array('changes_property'),
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => array('any'),
    ),
    'xmlsitemap_vbo_node_remove_from_xmlsitemap_action' => array(
      'type' => 'node',
      'label' => t('XML Sitemap Node: Exclude Node'),
      'behavior' => array('changes_property'),
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => array('any'),
    ),
    'xmlsitemap_vbo_node_reset_to_default_xmlsitemap_setting_action' => array(
      'type' => 'node',
      'label' => t('XML Sitemap Node: Reset to Default'),
      'behavior' => array('changes_property'),
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => array('any'),
    ),

    // VBO Actions for Taxonomy Terms.
    'xmlsitemap_vbo_taxonomy_add_to_xmlsitemap_action' => array(
      'type' => 'taxonomy_term',
      'label' => t('XML Sitemap Taxonomy: Include Term'),
      'behavior' => array('changes_property'),
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => array('any'),
    ),
    'xmlsitemap_vbo_taxonomy_remove_from_xmlsitemap_action' => array(
      'type' => 'taxonomy_term',
      'label' => t('XML Sitemap Taxonomy: Exclude Term'),
      'behavior' => array('changes_property'),
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => array('any'),
    ),
    'xmlsitemap_vbo_taxonomy_reset_to_default_xmlsitemap_setting_action' => array(
      'type' => 'taxonomy_term',
      'label' => t('XML Sitemap Taxonomy: Reset to Default'),
      'behavior' => array('changes_property'),
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => array('any'),
    ),

    // VBO Actions for Users.
    'xmlsitemap_vbo_user_add_to_xmlsitemap_action' => array(
      'type' => 'user',
      'label' => t('XML Sitemap User: Include'),
      'behavior' => array('changes_property'),
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => array('any'),
    ),
    'xmlsitemap_vbo_user_remove_from_xmlsitemap_action' => array(
      'type' => 'user',
      'label' => t('XML Sitemap User: Exclude'),
      'behavior' => array('changes_property'),
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => array('any'),
    ),
    'xmlsitemap_vbo_user_reset_to_default_xmlsitemap_setting_action' => array(
      'type' => 'user',
      'label' => t('XML Sitemap User: Reset to Default'),
      'behavior' => array('changes_property'),
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => array('any'),
    ),

  );
}

/**
 * Adding node from the xmlsitemap table.
 */
function xmlsitemap_vbo_node_add_to_xmlsitemap_action(&$node, $context) {
  // Setting message.
  $message = t('Node %title, %nid, has been included.', array(
    '%title' => $node->title,
    '%nid' => $node->nid,
  ));

  // Making update to the database.
  $nid = $node->nid;
  $query = db_update('xmlsitemap')
    ->fields(array(
      // Status to 1 so it is included in the sitemap.
      'status' => '1',
      // Overriding the default settings.
      'status_override' => '1',
    ))
    ->condition('type', 'node')
    ->condition('id', $nid, '=')
    ->execute();

  // Displaying confirmation message.
  drupal_set_message($message);
}

/**
 * Excluding node from the xmlsitemap table.
 */
function xmlsitemap_vbo_node_remove_from_xmlsitemap_action(&$node, $context) {
  // Setting message.
  $message = t('Node %title, %nid, has been removed.', array(
    '%title' => $node->title,
    '%nid' => $node->nid,
  ));

  // Making update to the database.
  $nid = $node->nid;
  $query = db_update('xmlsitemap')
    ->fields(array(
      // Status is set to 0 so it is not included in the sitemap.
      'status' => '0',
      // Overriding the default settings.
      'status_override' => '1',
    ))
    ->condition('type', 'node')
    ->condition('id', $nid, '=')
    ->execute();

  // Displaying confirmation message.
  drupal_set_message($message);
}

/**
 * Reseting node to the default xmlsitemap settings.
 */
function xmlsitemap_vbo_node_reset_to_default_xmlsitemap_setting_action(&$node, $context) {
  // Setting message.
  $message = t('Node %title, %nid, has been reset to default.', array(
    '%title' => $node->title,
    '%nid' => $node->nid,
  ));

  // Loading what the default status is per node.
  $vbo_settings = xmlsitemap_link_bundle_load('node', $node->type);
  $status = $vbo_settings['status'];

  // Making update to the database.
  $nid = $node->nid;
  $query = db_update('xmlsitemap')
    ->fields(array(
      // Resetting the status to default for the node.
      'status' => $status,
      // Reset the status override setting to default.
      'status_override' => '0',
    ))
    ->condition('type', 'node')
    ->condition('id', $nid, '=')
    ->execute();

  // Displaying confirmation message.
  drupal_set_message($message);
}

/**
 * Adding the Taxonomy Term from the xmlsitemap table.
 */
function xmlsitemap_vbo_taxonomy_add_to_xmlsitemap_action(&$term, $context) {
  // Setting message.
  $message = t('Term %title, %tid, has been included.', array(
    '%title' => $term->name,
    '%tid' => $term->tid,
  ));

  // Making update to the database.
  $tid = $term->tid;
  $query = db_update('xmlsitemap')
    ->fields(array(
      // Status to 1 so it is included in the sitemap.
      'status' => '1',
      // Overriding the default settings.
      'status_override' => '1',
    ))
    ->condition('type', 'taxonomy_term')
    ->condition('id', $tid, '=')
    ->execute();

  // Displaying confirmation message.
  drupal_set_message($message);
}

/**
 * Excluding Taxonomy Term from the xmlsitemap table.
 */
function xmlsitemap_vbo_taxonomy_remove_from_xmlsitemap_action(&$term, $context) {
  // Setting message.
  $message = t('Term %title, %tid, has been removed.', array(
    '%title' => $term->name,
    '%tid' => $term->tid,
  ));

  // Making update to the database.
  $tid = $term->tid;
  $query = db_update('xmlsitemap')
    ->fields(array(
      // Status is set to 0 so it is not included in the sitemap.
      'status' => '0',
      // Overriding the default settings.
      'status_override' => '1',
    ))
    ->condition('type', 'taxonomy_term')
    ->condition('id', $tid, '=')
    ->execute();

  // Displaying confirmation message.
  drupal_set_message($message);
}

/**
 * Reseting Taxonomy term to the default xmlsitemap settings.
 */
function xmlsitemap_vbo_taxonomy_reset_to_default_xmlsitemap_setting_action(&$term, $context) {
  // Setting message.
  $message = t('Term %title, %tid, has been reset to default.', array(
    '%title' => $term->name,
    '%tid' => $term->tid,
  ));

  // Loading what the default status is per node.
  $vbo_settings = xmlsitemap_link_bundle_load('taxonomy_term', $term->vocabulary_machine_name);
  $status = $vbo_settings['status'];

  // Making update to the database.
  $tid = $term->tid;
  $query = db_update('xmlsitemap')
    ->fields(array(
      // Resetting the status to default for the node.
      'status' => $status,
      // Reset the status override setting to default.
      'status_override' => '0',
    ))
    ->condition('type', 'taxonomy_term')
    ->condition('id', $tid, '=')
    ->execute();

  // Displaying confirmation message.
  drupal_set_message($message);
}

/**
 * Adding User to the xmlsitemap table.
 */
function xmlsitemap_vbo_user_add_to_xmlsitemap_action(&$user, $context) {
  // Setting message.
  $message = t('User %user, %uid, has been included.', array(
    '%user' => $user->name,
    '%uid' => $user->uid,
  ));

  // Making update to the database.
  $uid = $user->uid;
  $query = db_update('xmlsitemap')
    ->fields(array(
      // Status to 1 so it is included in the sitemap.
      'status' => '1',
      // Overriding the default settings.
      'status_override' => '1',
    ))
    ->condition('type', 'user')
    ->condition('id', $uid, '=')
    ->execute();

  // Displaying confirmation message.
  drupal_set_message($message);
}

/**
 * Excluding User from the xmlsitemap table.
 */
function xmlsitemap_vbo_user_remove_from_xmlsitemap_action(&$user, $context) {
  // Setting message.
  $message = t('User %user, %uid, has been removed.', array(
    '%user' => $user->name,
    '%uid' => $user->uid,
  ));

  // Making update to the database.
  $uid = $user->uid;
  $query = db_update('xmlsitemap')
    ->fields(array(
      // Status is set to 0 so it is not included in the sitemap.
      'status' => '0',
      // Overriding the default settings.
      'status_override' => '1',
    ))
    ->condition('type', 'user')
    ->condition('id', $uid, '=')
    ->execute();

  // Displaying confirmation message.
  drupal_set_message($message);
}

/**
 * Reseting User to the default xmlsitemap settings.
 */
function xmlsitemap_vbo_user_reset_to_default_xmlsitemap_setting_action(&$user, $context) {
  // Setting message.
  $message = t('User %user, %uid, has been reset to default.', array(
    '%user' => $user->name,
    '%uid' => $user->uid,
  ));

  // Loading what the default status is per node.
  $vbo_settings = xmlsitemap_link_bundle_load('user', 'user');
  $status = $vbo_settings['status'];

  // Making update to the database.
  $uid = $user->uid;
  $query = db_update('xmlsitemap')
    ->fields(array(
      // Resetting the status to default for the node.
      'status' => $status,
      // Reset the status override setting to default.
      'status_override' => '0',
    ))
    ->condition('type', 'user')
    ->condition('id', $uid, '=')
    ->execute();

  // Displaying confirmation message.
  drupal_set_message($message);
}

/**
 * Implements hook_views_api().
 */
function xmlsitemap_vbo_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'xmlsitemap_vbo') . '/views',
  );
}
